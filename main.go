package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"regexp"
	"strings"
	"time"
)

var RfcRegex = regexp.MustCompile(`(\d{4})-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])\s([01]\d|2[0-3]):[0-5]\d:[0-5]\d(\.\d+)?([+-]\d\d)`)
var inputFile = flag.String("input", "", "specify sql file to normalize timestamps")

var modifiedRFC3339Layout = `2006-01-02 15:04:05-07`

// Target format: 2023-05-30 16:01:20.094031+00

func normalize(in time.Time) (out time.Time, normalized bool) {
	_, zoneOffset := in.Zone()
	if zoneOffset != 0 {
		return in.UTC(), true
	}
	return in, false
}

func normalizeTimestampsStr(in string) string {
	matches := RfcRegex.FindAllString(in, -1)
	if matches == nil {
		return in
	}
	for _, v := range matches {
		inputTime, err := time.Parse(modifiedRFC3339Layout, v)
		if err != nil {
			panic(err)
		}
		normalizedTime, normalized := normalize(inputTime)
		if normalized {
			in = strings.Replace(in, v, normalizedTime.Format(modifiedRFC3339Layout), 1)
		}
	}
	return in
}

func main() {
	flag.Parse()
	inFile, err := os.Open(*inputFile)
	if err != nil {
		fmt.Println("Unable to process input file:", err)
		os.Exit(-1)
	}
	defer inFile.Close()
	scanner := bufio.NewScanner(inFile)
	for scanner.Scan() {
		fmt.Println(normalizeTimestampsStr(scanner.Text()))
	}
}
