# PostgreSQL Timestamp Normalizer

A small utility to find PostgreSQL-formatted timestamps in a SQL file and, if a timezone is specified, normalize to UTC. The file to normalize is given by the command line parameter `-input` and the output of the program is written to STDOUT.